#ifndef CIPHER_H_INCLUDED
#define CIPHER_H_INCLUDED

char* sanitizeInput(char* input);
int checkCaseType(char letter);
int cipher(char key, char clearChar, int offset);
int decipher(char key, char encryptedChar, int offset);

#endif // CIPHER_H_INCLUDED
