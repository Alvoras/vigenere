# vigenere
Cipher and decipher message encrypted with the vigenere cipher.

Usage : ./vigenere [-k \<key\>][-t \<text\>][-cd]

-c is used to get the encrypted text file, -d to get the decrypted text file.

The files are not modified.

The key and text option can either be a string or a text file.

 --- PLEASE NOTE --- 

Some uppercase letters are bugged. WIP
