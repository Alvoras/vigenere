#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "cipher.h"
#include "main.h"

char* readFile(FILE* file){
   char* text = NULL;
   int textlen = -1;

   // GET TEXT LENGTH
   fseek(file, 0L, SEEK_END);
   textlen = ftell(file);
   rewind(file);

   text = malloc(sizeof(char)*textlen+1);

   fread(text, textlen, sizeof(char), file);

   // ADD STRING TERMINATOR
   text[textlen] = 0x00;
   return text;
}

int main(int argc, char* argv[]) {

   FILE* keyFile = NULL;
   FILE* textFile = NULL;

   char* keyFilePath = NULL;
   char* textFilePath = NULL;
   char* key = NULL;
   char* text = NULL;

   int keyLength = -1;
   int cipherMode = -1;
   int i = 0;
   int padding = 0;
   int opt;
   int offset;

// a = 97
// z = 122
// 26 LETTERS
// +32 TO LOWER CASE

   if( !(argc > 1) ) printf("Usage : %s [-k <key>][-t <text>][-cd]\n", argv[0]), exit(EXIT_FAILURE);

   while ( (opt = getopt(argc, argv, "cdk:t:")) != -1 ) {
         switch (opt) {
            case 'c':
               cipherMode = 1;
               break;
            case 'd':
               cipherMode = 0;
               break;
            case 'k':
               keyFilePath = optarg;

               if( strcmp(strtok(argv[0], "./"), optarg) == 0 ) printf("\e[31mERROR \e[33mYou can't use the name of the program as your encryption key\n"), exit(EXIT_FAILURE);

               if ( (keyFile = fopen(keyFilePath, "r")) ) {
                  printf("Loading key file...\n");

                  key = readFile(keyFile);

                  fclose(keyFile);

                  key = sanitizeInput(key);
               }else{
                  key = optarg;
               }

               if ( strlen(key) == 0 ) printf("\n\e[31mERROR \e[33m\e[33mEmpty key file\e[0m\n"),
               exit(EXIT_FAILURE);

               break;
            case 't':
               textFilePath = optarg;

               if( strcmp(strtok(argv[0], "./"), optarg) == 0 ) printf("\e[31mERROR \e[33mYou can't use the name of the program as your text\n"), exit(EXIT_FAILURE);

               if ( (textFile = fopen(textFilePath, "r")) ) {
                  printf("Loading text file...\n");

                  text = readFile(textFile);

                  fclose(textFile);

                  text = sanitizeInput(text);
               }else{
                  text = optarg;
               }

               if ( strlen(text) == 0 ) printf("\n\e[31m ERROR \e[33mEmpty text file\e[0m\n"),
               exit(EXIT_FAILURE);

               break;
            default:
               printf("Usage : %s [-k <key>][-t <text>][-cd]\n", argv[0]);
               exit(EXIT_FAILURE);
               break;
         }
   }

   if (cipherMode == -1) {
      printf("\e[31mERROR \e[33mPlease use -c to encrypt the text or -d to decrypt the text\e[0m\n");
   }

   keyLength = strlen(key);

   printf("\n");

   // --- TODO PARSE TEXT FUNCTION ---
   switch (cipherMode) {
      case 0:
         while (text[i] != 0x00) {
            if ((text[i] >= 32 && text[i] <= 64) || (text[i] >= 91 && text[i] <= 96) || (text[i] >= 123 && text[i] <= 126)) {
               printf("%c", text[i]);
               i++;
               padding++;
               continue;
            }else if (text[i] == 10) { // IGNORE LINE FEED SYMBOL WHEN GETTING INPUT FROM FILE
              i++;
              continue;
            }

            offset = checkCaseType(text[i]);

            printf("%c", decipher(key[(i-padding)%keyLength], text[i], offset));
            i++;
         }
         break;
      case 1:
         while (text[i] != 0x00) {
            if ( (text[i] >= 32 && text[i] <= 64) || (text[i] >= 91 && text[i] <= 96) || (text[i] >= 123 && text[i] <= 126) ) {
               printf("%c", text[i]);
               i++;
               padding++;
               continue;
           }/*else if (text[i] == 10) { // IGNORE LINE FEED SYMBOL WHEN GETTING INPUT FROM FILE
             i++;
            continue;
        }*/

            offset = checkCaseType(text[i]);

            // DEBUG
            //printf("%d > %d == %d : ", text[i], key[(i-padding)%keyLength], cipher(key[(i-padding)%keyLength], text[i], offset));

            printf("%c", cipher(key[(i-padding)%keyLength], text[i], offset));
            i++;
         }
         break;
         default:
            printf("\e[31mERROR \e[33mCipher mode not initialised, exiting...\e[0m\n");
            exit(EXIT_FAILURE);
            break;
   }
   printf("\n");

   return 0;
}
