#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "cipher.h"

char* sanitizeInput(char* input){
  char* cleanInput = NULL;
  int keylen = 0;
  int cnt = 0;
  int newlen = 0;

  keylen = strlen(input);

  for (size_t i = 0; i < keylen; i++) {
    if (input[i] == 10) {
      cnt++;
    }
  }

  newlen = (keylen+1)-cnt;

  cleanInput = malloc(sizeof(char)*newlen);

  for (size_t i = 0; i < keylen; i++) {
    if (input[i] != 10) {
      cleanInput[i] = input[i];
    }
  }
  cleanInput[keylen-1] = 0x00;

  return cleanInput;
}
int checkCaseType(char letter){
   int offset = -1;

   if (letter >= 97 && letter <= 122) { // LOWERCASE
      offset = 97;
   }else if (letter >= 65 && letter <= 90) { // UPPERCASE
      offset = 71;
  }else{ // SPECIAL CHAR
    /*  printf("\n\e[33mSpecial char are not supported yet !\e[0m\n");
      exit(EXIT_FAILURE);*/
      //offset = 0;
   }

   return offset;
}
int cipher(char key, char clearChar, int offset){
   int encryptedChar;

   // Chiffré[i] = (Texte[i] + Clés[i]) modulo 26
   encryptedChar = ( ( ( key-offset) + ( clearChar-offset ) ) %26 ) + offset;

   return encryptedChar;
}
int decipher(char key, char encryptedChar, int offset){
   int decryptedChar;

   // Texte[i] = (Chiffré[i] - Clés[i]) modulo 26
   // WE ADD 26 IN CASE OF THE encryptedChar GOING NEGATIVE WHEN SUBSTRACTING THE OFFSET
   // SINCE WE ADD AS MUCH AS THE MODULO, THE OFFSET IS NULLYFIED
   decryptedChar = ( ( ( encryptedChar-offset+26 ) - ( key-offset ) ) %26 ) + offset;

   return decryptedChar;
}
